
# IX-API Website

We generate our website with [hugo](https://gohugo.io/), a static site
generator.


[[_TOC_]]

## Development

1. install hugo  (e.g. `brew install hugo`)
2. run `make`

You should now have a hugo dev server running
under [http://localhost:1313/](http://localhost:1313/).

### Using containers

Just run `make -f Makefile.containers`

You can set the container engine by passing a `DOCKER` variable:

    make -f Makefile.containers production DOCKER=podman

### Editing and Creating Content

The main sections ("How it works", "API Documentation" and "About IX-API"),
are located in `src/content`.

All content represented as markdown files.

The [Content-Management](https://gohugo.io/content-management/)
documentation goes much further into details on
how to create content using hugo.


#### Frontpage

The frontpage is an exception to the other content and is
located in `src/layouts/index.html`.

#### Navigation

The navigation is part of the theme and for now
is not generated automatically.

To edit the top-navigation edit the file:
`src/themes/ix-api/layouts/partials/menu.html`.

#### Assets

Images and other content related assets are located in
`src/assets/`.

The custom *frontmatter* property `contentimage` is used
to control the layout and include an image accompanying
the content.

To use `src/assets/images/stock4.jpg` you can set the
`contentimage` to `images/stock4.jpg`.

These assets are also accessible through resources:

`{{(resources.Get "images/stock3.svg").RelPermalink}}`


### Layout / Theme

The ix-api theme (`src/themes/ix-api`) contains all theme related
assets and html partials.

The layout is mostly extracted from the old wordpress site.


## Production

To create a production build in `./build/` just run

    make production

### Using containers

    make -f Makefile.containers production

## Deployment

We have prepared a container with an nginx serving
static content.

You can create the image you can run

    docker build -t ix-api-website:latest -f containers/deploy/Containerfile .

and then serve it through

    docker run -p8999:80 ix-api-website:latest

The image is built using gitlab-ci and can be found in the
container registry:

    registry.gitlab.com/ix-api/ix-api-website/live/ix-api-website:latest

