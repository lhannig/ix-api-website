---
title: "Press"
layout: text
parent: /about-ix-api
---

**16 December 2021**
## IX-API Gains Momentum as Partners Build IX-API Ecosystem

Last month, the IX-API founders [DE-CIX](https://www.de-cix.net/), [AMS-IX](https://www.ams-ix.net/ams) and [LINX](https://www.linx.net/) organised a virtual consultation workshop for other global IXPs, member networks and customers. The session was a success and created a positive platform to share ideas, answer queries and as a working group, to take improvement recommendations away to work on.

In 2019, the world’s leading Internet Exchange Point (IXP) operators AMS-IX (Amsterdam), DE-CIX (Frankfurt) and LINX (London), launched version 1 of the IX-API, a universal application-programming interface. This was a revolutionary collaboration for the IXPs, with the API being a first of its kind for the industry. The IX-API was initially adopted and utilised by the founders and a small handful of their common partners.

Version 2.1 was released this summer with the clear principle to drive wider adoption, and to benefit all types of IXPs, customers and members. This version updated schemas and the product offerings catalogue while at the same time included support for port management (incl. LOA handling), service decommissioning and account deprovisioning.

The goal of the workshop in November was to get feedback from the community for new features for a version 3.0 of IX-API. In addition, the workshop was a great platform to update the wider community on progress of the project and the cause.

LINX’s CTO Richard Petrie reflected; “It is important that whist we build more functionality into the IX-API we maintain good documentation, ease of adoption and scalability in the model, so that other IXPs can benefit from the development work we have done and continue to do.”

The first adopters of the IX-API attended the workshop and shared their user stories. The outcome of this was valuable feedback that has now been captured and put into a specification document that will become a working project over the next few months.

DE-CIX CTO Thomas King stated; “Now we have developed the foundations, driving additional functionality that our customers need is the highest on our priority list. This is supporting cloud connectivity and making monitoring and statistics data available for customers. Additionally, we are supporting the integration of the IX-API in third party open-source tools such as Peering Manager.”

The discussion of wider adoption and the ease to deploy remains high priority. AMS-IX, DE-CIX and LINX already have other contributors to the IX-API discussion and model, through IXPs such as [Netnod](https://www.netnod.se/) (Sweden) and [IX.BR](https://ix.br/) (Brazil) but they continue to look at how they can further support global IXPs of all sizes, to benefit from this project.

Ruben van den Brink, the AMS-IX CTO made this clear in his view on open standards; “As we drive development at AMS-IX and the other IXs we look to progressively share our experience, development models and tooling in an open and standards-based way so we can foster greater community benefit and trust in the IX-API in the long term”.

Most large global networks connect to anything from 10, 20 to 30 or more Internet Exchange Points around the world. The founders realise that they need a model and a solution that can be readily adopted so the customer experience is seamless and efficient across each and all IXPs.

Independent development work is running in parallel with the open-source BGP session management solution, Peering Manager. This promises to provide open-source tools to enhance the peering process that will ultimately integrate with the IX-API.

On 30th November Peering Manager released version 1.5, which is now able to retrieve data from IX-API instances for IXPs. It displays data found in the remote IX-API that matches what Peering Manager knows about the IXP. Missing connections to an IXP, if found in IX-API can be setup with few clicks thanks to this feature.

All in all, work on IX-API is full steam ahead and confident that 2022 will bring wider adoption and further progress so stay tuned!

---

**6 October 2020**
## Global Internet Exchange Leaders Release Next Phase of Universal API

**Enhanced IX API to include cloud connection capabilities, private
VLAN and closed user groups**

One year ago, the world’s leading Internet Exchange operators
[AMS-IX](https://www.ams-ix.net/) (Amsterdam),
[DE-CIX](https://www.de-cix.net/) (Frankfurt) and
[LINX](https://www.linx.net/) (London), launched a new universal
application-programming interface, the IX-API. The large proportion
of member, customer and partner crossover between all three
exchanges provided the opportunity to work with and offer something
to the community, making a new universal provisioning and
configuration API a natural yet revolutionary collaboration between
the three IXs.

The IX-API improves productivity and provisioning of services for
their immediate customers, members, and the wider networking
community to self-manage their existing and new interconnection
services, such as peering, more effectively. During the virtual
European Peering Forum (EPF) event in September, the release of
version 2 was presented. The IX-API version 2 contains a number of
new features including cloud connectivity capabilities, private
VLANs and closed user groups. The IX-API version 2 specification and
documentation is available now in [the sandbox on
https://ix-api.net/](https://ix-api.net/api-documentation/)

Dr Thomas King, Chief Technology Officer at DE-CIX says, “DE-CIX
implemented the DE-CIX API in September last year, right after
IX-API version 1 was released. Many customers and especially peering
resellers jumped at the chance, as IX-API version 1 mainly focused
on the peering use case. With IX-API version 2, cloud connectivity
is now also supported, and has already been implemented. The first
customers are already using version 2 to automatically provision and
configure dedicated cloud connectivity to cloud service providers
such as Microsoft Azure, Amazon AWS, and the Google Cloud platform.
Customers can now programmatically set up and control not only
peering services but also cloud connectivity to a large number of
cloud service providers. This transforms the DE-CIX interconnection
ecosystem into a software-defined Internet and Cloud Exchange.”

Richard Petrie, Chief Technology Officer for LINX adds: “For LINX
and version 2 we are working to deliver this in parallel to our
product offerings, as we enable cloud solutions through our portal
and enhanced interconnection services this year. Developing a wider
set of products and services for our members and partners is very
much part of the LINX strategy and IX-API version 2 provides a
programmable interface into our exchange platforms around the world.
We also continue to promote adoption across the IX community, to
help promote digital consumption of peering services in general.”

Henk Steenman, Chief Technology Officer for AMS-IX says: “Over the
last months we continued working on the IX-API to further improve
this service and make it easier for IX partners to interface with
IXs in a standardized, automated way. This will significantly
streamline the workflow of customers seeking to connect to an IX.”

Mutual connectivity and data centre partners
[Epsilon](https://www.epsilontel.com/) and
[Interxion](https://www.interxion.com/) were approached to trial the
first phase of the IX-API and will continue to trial and support
version 2 of the IX-API.

Further phases are being planned to include additional monitoring
and statistical functions. Find out more about the IX-API
collaboration project and live sandbox tools on this website:
[https://ix-api.net/](https://ix-api.net/)

---

**17 September 2019**

## For the ‘Good of the Internet’ 

**Global Internet Exchange Leaders Develop Universal API**

The world’s leading Internet Exchange operators
[AMS-IX](https://www.ams-ix.net/) (Amsterdam),
[DE-CIX](https://www.de-cix.net/) (Frankfurt) and
[LINX](https://www.linx.net/) (London) have joined forces to develop
a common Application Programming Interface (API) to provision and
configure interconnection services. This so-called IX-API will
improve productivity for their members, customers and partners
alike. It allows users to self-manage their existing and new
Interconnection services such as peering more effectively, from
ordering new ports to monitoring provisioning processes.

The consortium of the three Internet Exchanges also called on common
member and connectivity partner Epsilon
([www.epsilontel.com](https://www.epsilontel.com/)) and data centre
partner Interxion ([www.interxion.com](https://www.interxion.com/))
to act as pilot customers for the new software. Both partners’ tests
came out with positive results and they will be joining the
exchanges at the European Peering Forum (EPF) taking place in
September to present and demonstrate the tool to the rest of the
Internet and telecommunications community.

Dr Thomas King, Chief Technology Officer at DE-CIX says; “DE-CIX
ambition has always been to make customers life easier and connect
them anywhere needed on the planet. Therefore, we are pleased to
have come together with our partners to create this IX-API from
scratch. We are convinced that this new industry standard will be
adopted by other Internet Exchanges and interconnection providers.”

Richard Petrie, Chief Technology Officer for LINX adds; “Our
software and engineering teams have collaborated, working hard to
create this modern RESTful API based on OpenAPI Specification v3 for
managing IXPs customers and services.”

Henk Steenman, Chief Technology Officer for AMS-IX says; “At AMS-IX,
we are very proud that the three largest exchanges join forces,
collaborate and have reached consensus to deliver the common IX-API
project. In a very competitive market, we have found a common
ambition and drive for the greater good of the internet.”

There is a large percentage of member and customer crossover between
all three exchanges, working with the same remote peering partners
and data centres. Being able to work with and offer out to the
community, a universal API tool is a huge step forward for the
industry.

AMS-IX, DE-CIX and LINX are part of the European Peering Forum (EPF)
and aim to be unveiling the new project, in its completion, to their
industry peers at EPF14 this September in Tallinn, Estonia.

