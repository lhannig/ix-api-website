---
title: "Background Story" 
parent: /about-ix-api
layout: text
---

## Collaboration for the greater good of the Internet

In the past years, companies connected to IXs have developed a huge demand for
an API in order to support their businesses and the buildup of their network
infrastructure. Interconnection regimes have become more complex – and networks
have a high demand for faster planning and deployment of infrastructure in our
day-to-day business.

Therefore, AMS-IX, DE-CIX and LINX have formed a collaboration for the greater
good of the Internet and to accommodate these evolved circumstances. From now
on, companies which want to connect and configure services to an IX don’t have
to handle numerous different APIs.

## An industry standard to simplify IX services

To meet these new business demands, IX-API provides an industry standard
allowing programmability of 
[all participating IXs](https://ix-api.net/about-ix-api/participating-ixs/).
It provides a
consistent way of consuming peering, point-to-point connections,
cloud-connections and more.

The goal of IX-API is to provide IX customers and IXs with a common interface
for provisioning key services at multiple Internet Exchanges – and by that a
simpler platform for customers and an easier way to connect.

By providing one standard API language to multiple exchanges, implementation
costs for customers can be lowered drastically.

## Automated processes and increased efficiency

With IX-API, we will overcome the manual provisioning of interconnection, it
can be error-prone and time-consuming. The goal is to reduce errors in the
process as well as to reduce pressure on network engineers, cloud architects
and eventually all staff members.

To meet the new business demands, it is necessary to digitize the provisioning
processes. IX-API is a big step into the future of IXs and IX customers
worldwide.


