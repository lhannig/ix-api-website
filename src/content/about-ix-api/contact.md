---
title: "Contact"
parent: /about-ix-api
contentimage: images/stock8.jpg
---

If you have any questions about IX-API in general or on how to
implement it, either as Internet Exchange or as a network that wants
to configure its IX services, feel free to contact the
[participating Internet Exchanges](/about-ix-api/participating-ixs/)
at any time.

## Your feedback is highly appreciated

You can submit proposals on how to enhance the IX-API specification
via the [gitlab issue tracker](https://gitlab.com/ix-api/ix-api-sandbox/issues) 
at any time. The better and the more detailed the description of the
proposal is, the easier it is to select it.

The [IX-API board](/about-ix-api/ix-api-board-and-governance-model/)
will review proposals and invite the party to work on the
enhancement proposed. The party is expected to assign resources to
get the enhancement worked into the IX-API specification.

If you have any feedback you want to share, please contact 
[the participating IXs](/about-ix-api/participating-ixs/)
directly. Your feedback is highly appreciated.

