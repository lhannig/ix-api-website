---
title: "About IX-API"
contentimage: "images/stock6.jpg"
---

IX-API is an Application Programming Interface for provisioning key services at
multiple Internet Exchanges and supports fully end-to-end automated processes
to enable networks to configure, change and cancel IX services.

## By The Community, For The Community 

IX-API was designed and developed by
AMS-IX, DE-CIX and LINX, the three leading global Internet Exchanges. To meet
new business demands of Internet Exchanges and their customers, IX-API provides
an open industry standard allowing programmability of all participating IXs,
providing a consistent way of consuming peering, point-to-point connections,
cloud-connections and additional services.

{{< linkBtn href="/about-ix-api/background-story/" >}}Background story{{< /linkBtn >}}
